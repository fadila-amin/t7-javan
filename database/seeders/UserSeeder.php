<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'email' => 'fadloer@gmail.com',
            'name' => 'Fadlur',
            'password' => \Hash::make('123123'),
            'status' => 'aktif',
        ]);
    }
}
