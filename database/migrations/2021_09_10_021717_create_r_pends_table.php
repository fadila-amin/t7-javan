<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRPendsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('r_pends', function (Blueprint $table) {
            $table->id();
            $table->foreignId('id_dosen')->constrained('dosens');
            $table->string('strata',10);
            $table->string('jurusan');
            $table->string('sekolah');
            $table->year('tahun_mulai');
            $table->year('tahun_selesai');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('r_pends');
    }
}
