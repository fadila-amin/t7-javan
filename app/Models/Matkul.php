<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Matkul extends Model
{
    protected $fillable = [
        'nama',
        'sks'
    ];
    public function mhs(){
        return $this->belongsToMany(Mahasiswa::class,'kelas','id_matkul','id_mahasiswa')->withPivot('nama_kelas');
    }
}
