<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Dosen;

class R_pend extends Model
{
    protected $fillable = [
        'id_dosen',
        'strata',
        'jurusan',
        'sekolah',
        'tahun_mulai',
        'tahun_selesai'
    ];

    public function dosen(){
        return $this->belongsTo(Dosen::class);
    }
}
