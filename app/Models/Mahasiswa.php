<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mahasiswa extends Model
{
    protected $fillable = [
        'nama',
        'NIM',
        'jenis_kelamin',
        'TTL'
    ];
    public function kelas(){
        return $this->belongsToMany(Matkul::class,'kelas','id_mahasiswa','id_matkul')->withPivot('nama_kelas');
    }
    public function jumlah_sks(){
        $kelas = $this->kelas;
        $count = 0;
        if(isset($kelas)){
            foreach ($kelas as $value) {
                $count += $value->sks;
            }
        }
        return $count;
    }
}
