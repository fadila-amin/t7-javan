<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\R_pend;

class Dosen extends Model
{
    protected $fillable =[
        'nama',
        'NIP',
        'gelar'
    ];
    public function r_pend(){
        return $this->hasMany(R_pend::class);
    }
}
