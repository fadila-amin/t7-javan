<?php

namespace App\Http\Controllers;

use App\Models\Matkul;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class MatkulController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mk = Matkul::all();
        return response()->json([
            'message' => 'success',
            'data' => $mk
        ], Response::HTTP_OK);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $request->validate([
            'nama' => 'required',
            'sks' => 'required|integer'
        ]);

        try{
            $mk = Matkul::create($request->all());
            return response()->json([
                'message' => 'success',
                'data' => $mk
            ], Response::HTTP_OK);
        } catch(QueryException $e){
            return response()->json([
                'message' => "Gagal".$e->getMessage()
            ],Response::HTTP_NOT_IMPLEMENTED);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $mk = Matkul::findOrFail($id);
        return response()->json([
            'message' => 'success',
            'data' => $mk,
            'mahasiswa' => $mk->mhs
        ], Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $mk = Matkul::findOrFail($id);
        $validator = $request->validate([
            'nama' => 'required',
            'sks' => 'required|integer'
        ]);
        try{
            $mk->update($request->all());
            return response()->json([
                'message' => 'success',
                'data' => $mk
            ], Response::HTTP_OK);
        } catch(QueryException $e){
            return response()->json([
                'message' => "Gagal".$e->getMessage()
            ], Response::HTTP_NOT_IMPLEMENTED);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mk = Matkul::findOrFail($id);
        try {
            $mk->delete();
            $response = [
                'massage' => 'data telah dihapus'
            ];
            return response()->json($response, Response::HTTP_OK);
        } catch (QueryException $e) {
            return response()->json([
                'message' => "Gagal".$e->getMessage()
            ],Response::HTTP_NOT_IMPLEMENTED);
        }
    }
}
