<?php

namespace App\Http\Controllers;

use App\Models\R_pend;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class R_pendController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $r_pend = R_pend::all();
        return response()->json([
            'message' => 'success',
            'data' => $r_pend
        ], Response::HTTP_OK);
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = $request->validate([
            'strata' => 'required',
            'jurusan' => 'required',
            'sekolah' => 'required',
            'tahun_mulai' => 'required',
            'tahun_selesai' => 'required'
        ]);

        try{
            $r_pend = R_pend::create($request->all());
            $response = [
                'message' => 'Data telah ditambah',
                'data' => $r_pend
            ];
            return response()->json($response, Response::HTTP_OK);
        } catch(QueryException $e){
            return response()->json([
                'message' => "Gagal".$e->getMessage()
            ],Response::HTTP_NOT_IMPLEMENTED);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $r_pend = R_pend::findOrFail($id);
        $response = [
            'massage' => 'succes',
            'data' => $r_pend,
            'dosen' => $r_pend->dosen
        ];
        return response()->json($response, Response::HTTP_OK);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $r_pend = R_pend::findOrFail($id);
        $validation = $request->validate([
            'strata' => 'required',
            'jurusan' => 'required',
            'sekolah' => 'required',
            'tahun_mulai' => 'required',
            'tahun_selesai' => 'required'
        ]);

        try{
            $r_pend->update($request->all());
            $response = [
                'massage' => 'succes',
                'data' => $r_pend,
                'dosen' => $r_pend->dosen
            ];
            return response()->json($response, Response::HTTP_OK);
        }catch (QueryException $e) {
            return response()->json([
                'message' => "Gagal".$e->getMessage()
            ],Response::HTTP_NOT_IMPLEMENTED);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $r_pend = R_pend::findOrFail($id);

        try {
            $r_pend->delete();
            $response = [
                'massage' => 'data telah dihapus'
            ];
            return response()->json($response, Response::HTTP_OK);
        } catch (QueryException $e) {
            return response()->json([
                'message' => "Gagal".$e->getMessage()
            ],Response::HTTP_NOT_IMPLEMENTED);
        }
    }
}
