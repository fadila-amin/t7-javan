<?php

namespace App\Http\Controllers;

use App\Models\Dosen;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class DosenController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dosen = Dosen::all();
        return response()->json([
            'message' => 'success',
            'data' => $dosen
        ], Response::HTTP_OK);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = $request->validate([
            'nama' => 'required|string',
            'NIP' => 'required',
            'gelar' => 'required'
        ]);

        try {
            $dosen = Dosen::create($request->all());
            $response = [
                'message' => 'Data dosen telah ditambahkan',
                'data' => $dosen
            ];
            return response()->json($response,Response::HTTP_OK);
        } catch (QueryException $e) {
            return response()->json([
                'message' => "Gagal".$e->getMessage()
            ],Response::HTTP_NOT_IMPLEMENTED);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dosen = Dosen::findOrFail($id);
        try {
            $r_pend = $dosen->r_pend;
        } catch (\Throwable $th) {
            $r_pend = 'none';
        }
        $response = [
            'massage' => 'succes',
            'data' => $dosen,
            'r_pend' => $r_pend
        ];
        return response()->json($response,Response::HTTP_OK);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $dosen = Dosen::findOrFail($id);
        $validation = $request->validate([
            'nama' => 'required|string',
            'NIP' => 'required',
            'gelar' => 'required'
        ]);
        try {
            $dosen->update($request->all());
            $response = [
                'massage' => 'data telah diupdate',
                'data' => $dosen
            ];
            return response()->json($response, Response::HTTP_OK);
        } catch (QueryException $e) {
            return response()->json([
                'message' => "Gagal".$e->getMessage()
            ],Response::HTTP_NOT_IMPLEMENTED);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dosen = Dosen::findOrFail($id);

        try {
            $dosen->delete();
            $response = [
                'massage' => 'data telah dihapus'
            ];
            return response()->json($response, Response::HTTP_OK);
        } catch (QueryException $e) {
            return response()->json([
                'message' => "Gagal".$e->getMessage()
            ],Response::HTTP_NOT_IMPLEMENTED);
        }
    }
}
