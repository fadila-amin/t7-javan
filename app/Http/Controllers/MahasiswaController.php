<?php

namespace App\Http\Controllers;

use App\Models\Mahasiswa;
use App\Models\Matkul;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class MahasiswaController extends Controller
{
    public function submit_kelas(Request $request){
        $validator = $request->validate([
            'id_mhs' => 'required',
            'id_matkul' => 'required',
            'nama_kelas' => 'required'
        ]);
        $id_mhs = $request->input('id_mhs');
        $id_matkul = $request->input('id_matkul');
        $nama_kelas = $request->input('nama_kelas');

        $mk = Matkul::find($id_matkul);
        $mhs = Mahasiswa::find($id_mhs);
        try{
            if($mk->sks + $mhs->jumlah_sks() <=24){
                $mhs->kelas()->attach($id_matkul,['nama_kelas' => $nama_kelas]);
                return response()->json([
                    'message' => 'success',
                ], Response::HTTP_OK);
            } else {
                return response()->json([
                    'message' => 'Lebih dari 24sks',
                ], Response::HTTP_NOT_IMPLEMENTED);
            }
        } catch(QueryException $e){
            return response()->json([
                'message' => "Gagal".$e->getMessage()
            ],Response::HTTP_NOT_IMPLEMENTED);
        }
    }


    public function batal_kelas(Request $request){
        $validator = $request->validate([
            'id_mhs' => 'required|integer',
            'id_matkul' => 'required|integer',
        ]);
        $id_mhs = $request->input('id_mhs');
        $id_matkul = $request->input('id_matkul');

        $mhs = Mahasiswa::find($id_mhs);
        try{
                $mhs->kelas()->detach($id_matkul);
                return response()->json([
                    'message' => 'success',
                ], Response::HTTP_OK);
        } catch(QueryException $e){
            return response()->json([
                'message' => "Gagal".$e->getMessage()
            ],Response::HTTP_NOT_IMPLEMENTED);
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mhs = Mahasiswa::all();
        return response()->json([
            'message' => 'success',
            'data' => $mhs
        ],Response::HTTP_OK);
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = $request->validate([
            'nama' => 'required',
            'NIM' => 'required',
            'jenis_kelamin' => 'required',
            'TTL' => 'required'
        ]);

        try {
            $mhs = Mahasiswa::create($request->all());
            $response = [
                'message' => 'success',
                'data' => $mhs
            ];
            return response()->json($response, Response::HTTP_OK);
        } catch (QueryException $th) {
            return response()->json([
                'message' => "Gagal".$th->getMessage()
            ],Response::HTTP_NOT_IMPLEMENTED);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $mhs = Mahasiswa::findOrFail($id);
        $response  = [
            'message' => 'success',
            'data' => $mhs,
            'kelas' => $mhs->kelas
        ];
        return response()->json($response, Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $mhs = Mahasiswa::findOrFail($id);
        $validation = $request->validate([
            'nama' => 'required',
            'NIM' => 'required',
            'jenis_kelamin' => 'required',
            'TTL' => 'required'
        ]);

        try{
            $mhs->update($request->all());
            $response = [
                'massage' => 'succes',
                'data' => $mhs
            ];
            return response()->json($response, Response::HTTP_OK);
        }catch (QueryException $e) {
            return response()->json([
                'message' => "Gagal".$e->getMessage()
            ],Response::HTTP_NOT_IMPLEMENTED);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mhs = Mahasiswa::findOrFail($id);

        try {
            $mhs->delete();
            $response = [
                'massage' => 'data telah dihapus'
            ];
            return response()->json($response, Response::HTTP_OK);
        } catch (QueryException $e) {
            return response()->json([
                'message' => "Gagal".$e->getMessage()
            ],Response::HTTP_NOT_IMPLEMENTED);
        }
    }
}
