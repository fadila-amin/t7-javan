<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'AuthController@login');
Route::group(['middleware' => 'auth:sanctum'], function(){
    Route::post('/submit-kelas', 'MahasiswaController@submit_kelas');
    Route::post('/unsubmit-kelas', 'MahasiswaController@batal_kelas');
    Route::resource('/dosen', 'DosenController')->except(['create','edit']);
    Route::resource('/rpend', 'R_pendController')->except(['create','edit']);
    Route::resource('/mhs', 'MahasiswaController')->except(['create','edit']);
    Route::resource('/mk', 'MatkulController')->except(['create','edit']);
    Route::post('logout', 'AuthController@logout');
});
// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });
